public class RegistroEstudiantes{
	private String [] nombresEstudiantes;
	public RegistroEstudiantes(){
		nombresEstudiantes=new String[10];
	}
	public RegistroEstudiantes(int longitud){
		nombresEstudiantes=new String[longitud];
	}
	public void setNombre(int posicion, String nombre){
		nombresEstudiantes[posicion]=nombre;
	}
	public String getNombre(int posicion){
		return nombresEstudiantes[posicion];
	}
	public int length(){
		return nombresEstudiantes.length;
	}
}

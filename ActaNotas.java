public class ActaNotas{
	private double [] notas;
	
	public ActaNotas(){
		notas=new double[100];
	}
	public ActaNotas(int longitud){
		notas=new double[longitud];
	}
	public void setNota(int posicion, double nota){
		notas[posicion]=nota;
	}
	public double getNota(int posicion){
		return notas[posicion];
	}
	public int length(){
		return notas.length;
	} 
}
